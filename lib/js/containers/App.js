"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _Layout = _interopRequireDefault(require("../bootstrap-components/Layout"));

var _Routes = _interopRequireDefault(require("../components/Routes"));

var _Navigation = _interopRequireDefault(require("../bootstrap-components/Navigation"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var App = function App() {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_reactRouterDom.BrowserRouter, null, _react.default.createElement(_Navigation.default, null), _react.default.createElement(_Layout.default, null, _react.default.createElement(_Routes.default, null))));
};

var _default = App;
exports.default = _default;