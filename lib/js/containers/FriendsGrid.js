"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _redux = require("redux");

var _reactReduxFirebase = require("react-redux-firebase");

var _DeleteFriendAction = require("../actions/DeleteFriendAction");

var _SortFriendsAction = require("../actions/SortFriendsAction");

var _Friend = _interopRequireDefault(require("../bootstrap-components/Friend"));

var _SortButton = _interopRequireDefault(require("../bootstrap-components/SortButton"));

var _fbConfig = require("../config/fbConfig");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FriendsGrid = function FriendsGrid(_ref) {
  var friendList = _ref.friendList,
      deleteFriend = _ref.deleteFriend,
      sortFriends = _ref.sortFriends;
  return _react.default.createElement("ul", null, friendList.map(function (el) {
    return _react.default.createElement(_Friend.default, {
      id: el.id,
      key: el.id,
      name: el.firstName,
      lastName: el.lastName,
      age: el.age,
      deleteFriend: deleteFriend
    });
  }), _react.default.createElement(_SortButton.default, {
    className: "sort",
    sortFriends: sortFriends
  }));
}; // const mapStateToProps = state =>
//   // console.log(state);
//   console.log(state.firestore.ordered.friends);
// ({
//   friendList: state.friendList,
//   friends: state.firestore.ordered.friends,
//   isLoaded: state.firebase.profile.isLoaded,
// });


var mapStateToProps = function mapStateToProps(_ref2) {
  var friendList = _ref2.friendList,
      firestore = _ref2.firestore;
  return {
    friendList: friendList,
    friends: firestore.ordered.friends
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    deleteFriend: function deleteFriend(e) {
      e.persist();
      e.preventDefault();
      dispatch((0, _DeleteFriendAction.deleteFriend)(e.target.id));
    },
    sortFriends: function sortFriends(e) {
      e.preventDefault();
      dispatch((0, _SortFriendsAction.sortFriends)());
    }
  };
}; // export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(FriendsGrid);


var _default = (0, _redux.compose)((0, _reactReduxFirebase.firestoreConnect)([{
  collection: 'friends'
}]), (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps))(FriendsGrid); // db
//   .collection('friends')
//   .get()
//   .then(function (result) {
//     const emptyArr = [];
//     result.forEach(function (doc) {
//       emptyArr.push(doc.data());
//     });
//     emptyArr.map(el => (
//       <Friend
//         id={el.id}
//         key={el.id}
//         name={el.firstName}
//         lastName={el.lastName}
//         age={el.age}
//         deleteFriend={deleteFriend}
//       />
//     ));
//   })
//     }
//     <SortButton className="sort" sortFriends={sortFriends} />
//   </ul>
// );


exports.default = _default;