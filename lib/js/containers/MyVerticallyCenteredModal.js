"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactRedux = require("react-redux");

var _UpdateFriendAction = require("../actions/UpdateFriendAction");

var _SubmitUpdateAction = require("../actions/SubmitUpdateAction");

var _ModalFirstname = _interopRequireDefault(require("../bootstrap-components/ModalFirstname"));

var _ModalLastname = _interopRequireDefault(require("../bootstrap-components/ModalLastname"));

var _ModalAge = _interopRequireDefault(require("../bootstrap-components/ModalAge"));

var _ModalUpdateButton = _interopRequireDefault(require("../bootstrap-components/ModalUpdateButton"));

var _ModalHeader = _interopRequireDefault(require("../bootstrap-components/ModalHeader"));

var _ModalFooter = _interopRequireDefault(require("../bootstrap-components/ModalFooter"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var MyVerticallyCenteredModal =
/*#__PURE__*/
function (_React$Component) {
  _inherits(MyVerticallyCenteredModal, _React$Component);

  function MyVerticallyCenteredModal(props) {
    var _this;

    _classCallCheck(this, MyVerticallyCenteredModal);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(MyVerticallyCenteredModal).call(this, props));
    _this.id = props.id;
    _this.friendList = props.friendList;
    _this.submitUpdate = props.submitUpdate;
    _this.user = _this.friendList.filter(function (usr) {
      return usr.id === _this.id;
    });
    _this.updateAge = props.updateAge;
    _this.updateFirstName = props.updateFirstName;
    _this.updateLastName = props.updateLastName;
    return _this;
  }

  _createClass(MyVerticallyCenteredModal, [{
    key: "render",
    value: function render() {
      return _react.default.createElement(_reactBootstrap.Modal, _extends({}, this.props, {
        size: "lg",
        "aria-labelledby": "contained-modal-title-vcenter",
        centered: true
      }), _react.default.createElement(_ModalHeader.default, null), _react.default.createElement(_reactBootstrap.Modal.Body, null, _react.default.createElement("form", {
        action: "",
        onSubmit: this.submitUpdate,
        id: this.id
      }, _react.default.createElement(_ModalFirstname.default, {
        firstName: this.user[0].firstName,
        updateFirstName: this.updateFirstName
      }), _react.default.createElement(_ModalLastname.default, {
        lastName: this.user[0].lastName,
        updateLastName: this.updateLastName
      }), _react.default.createElement(_ModalAge.default, {
        age: this.user[0].age,
        updateAge: this.updateAge
      }), _react.default.createElement(_ModalUpdateButton.default, {
        id: this.id
      }))), _react.default.createElement(_ModalFooter.default, {
        hide: this.props.onHide
      }));
    }
  }], [{
    key: "propTypes",
    get: function get() {
      return {
        onHide: _propTypes.default.func,
        id: _propTypes.default.string,
        friendList: _propTypes.default.array,
        updateFirstName: _propTypes.default.func,
        updateLastName: _propTypes.default.func,
        updateAge: _propTypes.default.func,
        submitUpdate: _propTypes.default.func
      };
    }
  }]);

  return MyVerticallyCenteredModal;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(_ref) {
  var friendList = _ref.friendList;
  return {
    friendList: friendList
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    updateFirstName: function updateFirstName(e) {
      dispatch((0, _UpdateFriendAction.updateFirstName)(e.target.value));
    },
    updateLastName: function updateLastName(e) {
      dispatch((0, _UpdateFriendAction.updateLastName)(e.target.value));
    },
    updateAge: function updateAge(e) {
      dispatch((0, _UpdateFriendAction.updateAge)(parseInt(e.target.value, 10)));
    },
    submitUpdate: function submitUpdate(e) {
      e.preventDefault();
      e.persist();
      dispatch((0, _SubmitUpdateAction.submitUpdate)(e.target.id));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(MyVerticallyCenteredModal);

exports.default = _default;