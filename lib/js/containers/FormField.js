"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _redux = require("redux");

var _v = _interopRequireDefault(require("uuid/v1"));

var _reactReduxFirebase = require("react-redux-firebase");

var _AddFriendAction = require("../actions/AddFriendAction");

var _submitFriendAction = require("../actions/submitFriendAction");

var _AddButton = _interopRequireDefault(require("../bootstrap-components/AddButton"));

var _Firstname = _interopRequireDefault(require("../bootstrap-components/Firstname"));

var _Lastname = _interopRequireDefault(require("../bootstrap-components/Lastname"));

var _Age = _interopRequireDefault(require("../bootstrap-components/Age"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable no-shadow */
var FormField = function FormField(_ref) {
  var addFirstName = _ref.addFirstName,
      addLastName = _ref.addLastName,
      addAge = _ref.addAge,
      submitFriend = _ref.submitFriend;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("fieldset", null, _react.default.createElement("legend", null, "Add a friend"), _react.default.createElement("form", {
    action: "",
    onSubmit: submitFriend
  }, _react.default.createElement(_Firstname.default, {
    addFirstName: addFirstName
  }), _react.default.createElement(_Lastname.default, {
    addLastName: addLastName
  }), _react.default.createElement(_Age.default, {
    addAge: addAge
  }), _react.default.createElement(_AddButton.default, null))));
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    addFirstName: function addFirstName(e) {
      dispatch((0, _AddFriendAction.addFirstName)(e.target.value));
    },
    addLastName: function addLastName(e) {
      dispatch((0, _AddFriendAction.addLastName)(e.target.value));
    },
    addAge: function addAge(e) {
      dispatch((0, _AddFriendAction.addAge)(parseInt(e.target.value, 10)));
    },
    submitFriend: function submitFriend(e) {
      e.preventDefault();
      var id = (0, _v.default)();
      dispatch((0, _submitFriendAction.submitFriend)(id)); // dispatch(emptyForm());
    } // emptyForm: (e: *) => {
    //   e.preventDefault();
    //   dispatch(emptyForm());
    // },

  };
};

var _default = (0, _redux.compose)((0, _reactReduxFirebase.firestoreConnect)([{
  collection: 'friends'
}]), (0, _reactRedux.connect)(null, mapDispatchToProps))(FormField); // export default connect<State, Dispatch>(
//   null,
//   mapDispatchToProps
// )(FormField);


exports.default = _default;