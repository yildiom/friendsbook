"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormField = _interopRequireDefault(require("./FormField"));

var _FriendsGrid = _interopRequireDefault(require("./FriendsGrid"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Home = function Home() {
  return _react.default.createElement("div", {
    className: "main"
  }, _react.default.createElement(_FormField.default, null), _react.default.createElement(_FriendsGrid.default, null));
};

var _default = Home;
exports.default = _default;