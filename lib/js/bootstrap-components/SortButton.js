"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SortButton = function SortButton(_ref) {
  var sortFriends = _ref.sortFriends;
  return _react.default.createElement("div", {
    className: "sort"
  }, _react.default.createElement(_reactBootstrap.Button, {
    variant: "secondary",
    onClick: sortFriends
  }, "Sort"));
};

var _default = SortButton;
exports.default = _default;