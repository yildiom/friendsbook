"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

var _UpdateFriend = _interopRequireDefault(require("./UpdateFriend"));

var _DeleteButton = _interopRequireDefault(require("./DeleteButton"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Friend = function Friend(_ref) {
  var id = _ref.id,
      name = _ref.name,
      lastName = _ref.lastName,
      age = _ref.age,
      deleteFriend = _ref.deleteFriend;
  return _react.default.createElement("li", {
    key: id
  }, _react.default.createElement(_reactBootstrap.Card, null, _react.default.createElement(_reactBootstrap.Card.Body, null, _react.default.createElement(_reactBootstrap.Card.Title, null, _react.default.createElement("span", {
    className: "name"
  }, "Name: ", name, " ", lastName), _react.default.createElement("span", {
    className: "age"
  }, " Age: ", age)), _react.default.createElement("div", {
    className: "buttonHolder"
  }, _react.default.createElement(_DeleteButton.default, {
    deleteFriend: deleteFriend,
    id: id
  }), _react.default.createElement(_UpdateFriend.default, {
    id: id
  })))));
};

var _default = Friend;
exports.default = _default;