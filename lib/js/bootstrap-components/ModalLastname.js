"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ModalLastname = function ModalLastname(_ref) {
  var lastName = _ref.lastName,
      updateLastName = _ref.updateLastName;
  return _react.default.createElement(_reactBootstrap.Form.Group, {
    as: _reactBootstrap.Row,
    controlId: "formPlaintextPassword"
  }, _react.default.createElement(_reactBootstrap.Form.Label, {
    column: true,
    sm: "2"
  }, "Lastname"), _react.default.createElement(_reactBootstrap.Col, {
    sm: "10"
  }, _react.default.createElement(_reactBootstrap.Form.Control, {
    type: "lastname",
    placeholder: lastName,
    onChange: updateLastName
  })));
};

var _default = ModalLastname;
exports.default = _default;