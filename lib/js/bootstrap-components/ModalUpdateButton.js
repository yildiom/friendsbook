"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ModalUpdateButton = function ModalUpdateButton() {
  return _react.default.createElement(_reactBootstrap.Button, {
    variant: "primary",
    type: "submit"
  }, "Update");
};

var _default = ModalUpdateButton;
exports.default = _default;