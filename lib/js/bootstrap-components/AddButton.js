"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddButton = function AddButton(_ref) {
  var onClick = _ref.onClick;
  return _react.default.createElement(_reactBootstrap.Button, {
    variant: "primary",
    type: "submit",
    onClick: onClick,
    "data-test": "AddButton"
  }, "Add");
};

var _default = AddButton;
exports.default = _default;