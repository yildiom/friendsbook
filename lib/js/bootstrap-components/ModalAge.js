"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ModalAge = function ModalAge(_ref) {
  var age = _ref.age,
      updateAge = _ref.updateAge;
  return _react.default.createElement(_reactBootstrap.Form.Group, {
    as: _reactBootstrap.Row,
    controlId: "formPlaintextPassword"
  }, _react.default.createElement(_reactBootstrap.Form.Label, {
    column: true,
    sm: "2"
  }, "Age"), _react.default.createElement(_reactBootstrap.Col, {
    sm: "10"
  }, _react.default.createElement(_reactBootstrap.Form.Control, {
    type: "lastname",
    placeholder: age,
    onChange: updateAge
  })));
};

var _default = ModalAge;
exports.default = _default;