"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ModalFooter = function ModalFooter(_ref) {
  var hide = _ref.hide;
  return _react.default.createElement(_reactBootstrap.Modal.Footer, null, _react.default.createElement(_reactBootstrap.Button, {
    variant: "danger",
    onClick: hide,
    style: {
      fontFamily: 'Montserrat'
    }
  }, "Close"));
};

var _default = ModalFooter;
exports.default = _default;