"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ModalFirstname = function ModalFirstname(_ref) {
  var firstName = _ref.firstName,
      updateFirstName = _ref.updateFirstName;
  return _react.default.createElement(_reactBootstrap.Form.Group, {
    as: _reactBootstrap.Row,
    controlId: "formPlaintextPassword"
  }, _react.default.createElement(_reactBootstrap.Form.Label, {
    column: true,
    sm: "2"
  }, "Firstname"), _react.default.createElement(_reactBootstrap.Col, {
    sm: "10"
  }, _react.default.createElement(_reactBootstrap.Form.Control, {
    type: "firstname",
    placeholder: firstName,
    onChange: updateFirstName
  })));
};

var _default = ModalFirstname;
exports.default = _default;