"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DeleteButton = function DeleteButton(_ref) {
  var deleteFriend = _ref.deleteFriend,
      id = _ref.id;
  return _react.default.createElement(_reactBootstrap.Button, {
    variant: "danger",
    onClick: deleteFriend,
    id: id
  }, "Delete");
};

var _default = DeleteButton;
exports.default = _default;