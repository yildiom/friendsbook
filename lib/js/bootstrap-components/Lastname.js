"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Lastname = function Lastname(_ref) {
  var addLastName = _ref.addLastName;
  return _react.default.createElement(_reactBootstrap.Form.Group, {
    controlId: "formBasicPassword"
  }, _react.default.createElement(_reactBootstrap.Form.Label, null, "Lastname"), _react.default.createElement(_reactBootstrap.Form.Control, {
    type: "lastname",
    placeholder: "Enter Lastname",
    onChange: addLastName
  }));
};

var _default = Lastname;
exports.default = _default;