"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Firstname = function Firstname(_ref) {
  var addFirstName = _ref.addFirstName;
  return _react.default.createElement(_reactBootstrap.Form.Group, {
    controlId: "formBasicEmail"
  }, _react.default.createElement(_reactBootstrap.Form.Label, null, "Firstname"), _react.default.createElement(_reactBootstrap.Form.Control, {
    type: "firstname",
    placeholder: "Enter Firstname",
    onChange: addFirstName
  }));
};

var _default = Firstname;
exports.default = _default;