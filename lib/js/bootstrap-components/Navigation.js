"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _reactBootstrap = require("react-bootstrap");

var _reactRedux = require("react-redux");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NavigationBar = function NavigationBar() {
  return _react.default.createElement(_reactBootstrap.Container, null, _react.default.createElement(_reactBootstrap.Navbar, {
    expand: "lg",
    bg: "primary",
    variant: "dark"
  }, _react.default.createElement(_reactBootstrap.Navbar.Brand, null, _react.default.createElement(_reactRouterDom.Link, {
    to: "/"
  }, "Friendsbook")), _react.default.createElement(_reactBootstrap.Navbar.Toggle, {
    "aria-controls": "basic-navbar-nav"
  }), _react.default.createElement(_reactBootstrap.Navbar.Collapse, {
    id: "basic-navbar-nav"
  }, _react.default.createElement(_reactBootstrap.Nav, {
    className: "ml-auto"
  }, _react.default.createElement(_reactBootstrap.Nav.Item, null, _react.default.createElement(_reactRouterDom.Link, {
    to: "/"
  }, "Home")), _react.default.createElement(_reactBootstrap.Nav.Item, null, _react.default.createElement(_reactRouterDom.Link, {
    to: "/about"
  }, "About")), _react.default.createElement(_reactBootstrap.Nav.Item, null, _react.default.createElement(_reactRouterDom.Link, {
    to: "/contact"
  }, "Contact"))))));
}; // const mapStateToProps = state => {
//   console.log(state);
// };
// export default connect(mapStateToProps)(NavigationBar);


var _default = NavigationBar;
exports.default = _default;