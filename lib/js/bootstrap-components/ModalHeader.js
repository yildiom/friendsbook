"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ModalHeader = function ModalHeader() {
  return _react.default.createElement(_reactBootstrap.Modal.Header, {
    closeButton: true
  }, _react.default.createElement(_reactBootstrap.Modal.Title, {
    id: "contained-modal-title-vcenter"
  }, "Update Friend Info"));
};

var _default = ModalHeader;
exports.default = _default;