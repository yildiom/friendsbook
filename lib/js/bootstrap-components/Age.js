"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Age = function Age(_ref) {
  var addAge = _ref.addAge;
  return _react.default.createElement(_reactBootstrap.Form.Group, {
    controlId: "formBasicPassword"
  }, _react.default.createElement(_reactBootstrap.Form.Label, null, "Age"), _react.default.createElement(_reactBootstrap.Form.Control, {
    type: "age",
    placeholder: "Enter Age",
    onChange: addAge,
    "data-test": "ageComponent"
  }));
};

var _default = Age;
exports.default = _default;