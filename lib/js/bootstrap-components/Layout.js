"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactBootstrap = require("react-bootstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Layout = function Layout(props) {
  // eslint-disable-next-line react/prop-types
  var children = props.children;
  return _react.default.createElement(_reactBootstrap.Container, null, " ", children);
};

var _default = Layout;
exports.default = _default;