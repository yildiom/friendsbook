"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _About = _interopRequireDefault(require("./About"));

var _Home = _interopRequireDefault(require("../containers/Home"));

var _Contact = _interopRequireDefault(require("./Contact"));

var _Nomatch = _interopRequireDefault(require("./Nomatch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Routes = function Routes() {
  return _react.default.createElement(_reactRouterDom.Switch, null, _react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "/",
    component: _Home.default
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "/about",
    component: _About.default
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "/contact",
    component: _Contact.default
  }), _react.default.createElement(_reactRouterDom.Route, {
    component: _Nomatch.default
  }));
};

var _default = Routes;
exports.default = _default;