"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Contact = function Contact() {
  return _react.default.createElement("div", {
    "data-test": "contactComponent"
  }, _react.default.createElement("h2", null, "CONTACT"));
};

var _default = Contact;
exports.default = _default;