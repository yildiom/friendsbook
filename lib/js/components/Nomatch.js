"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NoMatch = function NoMatch() {
  return _react.default.createElement("div", null, _react.default.createElement("h2", null, "NO MATCH"));
};

var _default = NoMatch;
exports.default = _default;