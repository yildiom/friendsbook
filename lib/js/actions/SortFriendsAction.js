"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sortFriends = void 0;

var _actionsTypes = require("./actionsTypes");

var sortFriends = function sortFriends() {
  return {
    type: _actionsTypes.SORT_FRIENDS
  };
};

exports.sortFriends = sortFriends;