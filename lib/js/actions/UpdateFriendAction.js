"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateAge = exports.updateLastName = exports.updateFirstName = void 0;

var _actionsTypes = require("./actionsTypes");

var updateFirstName = function updateFirstName(updatedFirstname) {
  return {
    type: _actionsTypes.UPDATE_FIRSTNAME,
    payload: updatedFirstname
  };
};

exports.updateFirstName = updateFirstName;

var updateLastName = function updateLastName(updatedLastname) {
  return {
    type: _actionsTypes.UPDATE_LASTNAME,
    payload: updatedLastname
  };
};

exports.updateLastName = updateLastName;

var updateAge = function updateAge(updatedAge) {
  return {
    type: _actionsTypes.UPDATE_AGE,
    payload: updatedAge
  };
};

exports.updateAge = updateAge;