"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.emptyForm = exports.addAge = exports.addLastName = exports.addFirstName = void 0;

var _actionsTypes = require("./actionsTypes");

var addFirstName = function addFirstName(firstname) {
  return {
    type: _actionsTypes.ADD_FIRSTNAME,
    payload: firstname
  };
};

exports.addFirstName = addFirstName;

var addLastName = function addLastName(lastname) {
  return {
    type: _actionsTypes.ADD_LASTNAME,
    payload: lastname
  };
};

exports.addLastName = addLastName;

var addAge = function addAge(age) {
  return {
    type: _actionsTypes.ADD_AGE,
    payload: age
  };
};

exports.addAge = addAge;

var emptyForm = function emptyForm() {
  return {
    type: _actionsTypes.EMPTY_FORM
  };
};

exports.emptyForm = emptyForm;