"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteFriend = void 0;

var _actionsTypes = require("./actionsTypes");

var _fbConfig = require("../config/fbConfig");

var deleteFriend = function deleteFriend(id) {
  return function (dispatch) {
    var query = _fbConfig.db.collection('friends').where('id', '==', id);

    query.get().then(function (snapShot) {
      snapShot.forEach(function (doc) {
        doc.ref.delete();
      });
    }).then(function () {
      dispatch({
        type: _actionsTypes.DELETE_FRIEND,
        payload: id
      });
    });
  };
};

exports.deleteFriend = deleteFriend;