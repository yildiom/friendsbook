"use strict";

require("../css/style.scss");

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _reactRedux = require("react-redux");

var _store = require("./store/store");

var _App = _interopRequireDefault(require("./containers/App"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_reactDom.default.render(_react.default.createElement(_reactRedux.Provider, {
  store: _store.store
}, _react.default.createElement(_App.default, null)), document.getElementById('app'));