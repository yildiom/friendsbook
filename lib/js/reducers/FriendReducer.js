"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.friendReducer = void 0;

var _submitFriendState = require("../store/submitFriendState");

var _actionsTypes = require("../actions/actionsTypes");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var friendReducer = function friendReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _submitFriendState.submitFriendState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _actionsTypes.SUBMIT_FRIEND:
      return [].concat(_toConsumableArray(state), [action.payload]);

    case _actionsTypes.SUBMIT_UPDATE:
      return _toConsumableArray(state.map(function (el) {
        if (el.id === action.payload.id) {
          el.firstName = action.payload.firstName;
          el.lastName = action.payload.lastName;
          el.age = action.payload.age;
        }

        return el;
      }));

    case _actionsTypes.DELETE_FRIEND:
      return _toConsumableArray(state.filter(function (el) {
        return el.id !== action.payload;
      }));

    case _actionsTypes.SORT_FRIENDS:
      return _toConsumableArray(state.sort(function (a, b) {
        return a.age - b.age;
      }));

    default:
      return state;
  }
};

exports.friendReducer = friendReducer;