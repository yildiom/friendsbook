"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxFirestore = require("redux-firestore");

var _redux = require("redux");

var _reactReduxFirebase = require("react-redux-firebase");

var _AddFriendReducer = require("./AddFriendReducer");

var _FriendReducer = require("./FriendReducer");

var reducer = (0, _redux.combineReducers)({
  firestore: _reduxFirestore.firestoreReducer,
  firebase: _reactReduxFirebase.firebaseReducer,
  friend: _AddFriendReducer.friendInfoReducer,
  friendList: _FriendReducer.friendReducer
});
var _default = reducer;
exports.default = _default;