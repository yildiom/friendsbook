"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.db = void 0;

var _app = _interopRequireDefault(require("firebase/app"));

require("firebase/firestore");

require("firebase/auth");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: 'AIzaSyARg5rVBYe0Fo8Wyj2KizFke_dummX_DZk',
  authDomain: 'friendsbook-8ec44.firebaseapp.com',
  databaseURL: 'https://friendsbook-8ec44.firebaseio.com',
  projectId: 'friendsbook-8ec44',
  storageBucket: '',
  messagingSenderId: '454979822945',
  appId: '1:454979822945:web:a0e7f1637a1476af'
}; // Initialize Firebase

_app.default.initializeApp(firebaseConfig);

var db = _app.default.firestore();

exports.db = db;
var _default = _app.default;
exports.default = _default;