"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _UpdateFriend = _interopRequireDefault(require("../bootstrap-components/UpdateFriend"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<Update/>', function () {
  var addClick = jest.fn();
  var wrapper = (0, _enzyme.shallow)(_react.default.createElement(_UpdateFriend.default, {
    onClick: addClick
  })); // expect(addClick).NotToHaveBeenCalled();

  test('UpdateButton should render', function () {
    var tree = (0, _enzymeToJson.default)(wrapper);
    expect(tree).toMatchSnapshot();
  }); // test('AddButton should be clicked', () => {
  //   wrapper.find('Button').simulate('click');
  //   expect(addClick).toHaveBeenCalled();
  // });
});