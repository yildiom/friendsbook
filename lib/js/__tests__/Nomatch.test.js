"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _Nomatch = _interopRequireDefault(require("../components/Nomatch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<Nomatch />', function () {
  test('Nomatch component should render', function () {
    var noMatchShallow = (0, _enzyme.shallow)(_react.default.createElement(_Nomatch.default, null));
    var noMatchTree = (0, _enzymeToJson.default)(noMatchShallow);
    expect(noMatchTree).toMatchSnapshot();
  });
});