"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _index = require("../../Utils/index");

var _Age = _interopRequireDefault(require("../bootstrap-components/Age"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<Age/>', function () {
  var addAge;
  var wrapper;
  beforeEach(function () {
    addAge = jest.fn();
    wrapper = (0, _enzyme.shallow)(_react.default.createElement(_Age.default, {
      OnChange: addAge
    }));
  });
  test('Age field should render', function () {
    var tree = (0, _enzymeToJson.default)(wrapper);
    expect(tree).toMatchSnapshot();
  });
  it('Should have the test attribute', function () {
    var ageComponent = (0, _index.findByTestAttr)(wrapper, 'ageComponent');
    expect(ageComponent.length).toBe(1);
  });
});