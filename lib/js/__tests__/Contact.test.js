"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _Contact = _interopRequireDefault(require("../components/Contact"));

var _index = require("../../Utils/index");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import toJson from 'enzyme-to-json';
describe('<Contact />', function () {
  test('Contact component should render', function () {
    // const contactShallow = shallow(<Contact />);
    // const contactTree = toJson(contactShallow);
    // expect(contactTree).toMatchSnapshot();
    var wrapper = (0, _enzyme.shallow)(_react.default.createElement(_Contact.default, null));
    var contact = (0, _index.findByTestAttr)(wrapper, 'contactComponent');
    expect(contact.length).toBe(1);
  });
});