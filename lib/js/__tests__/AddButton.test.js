"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _index = require("../../Utils/index");

var _AddButton = _interopRequireDefault(require("../bootstrap-components/AddButton"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<AddButton/>', function () {
  var wrapper;
  var addClick;
  beforeEach(function () {
    addClick = jest.fn();
    wrapper = (0, _enzyme.shallow)(_react.default.createElement(_AddButton.default, {
      onClick: addClick
    }));
  });
  test('AddButton should render', function () {
    var tree = (0, _enzymeToJson.default)(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('Check if it has attribute', function () {
    var button = (0, _index.findByTestAttr)(wrapper, 'AddButton');
    expect(button.length).toBe(1);
  });
  it('Should call the click event', function () {
    var button = (0, _index.findByTestAttr)(wrapper, 'AddButton');
    button.simulate('click');
    var callback = addClick.mock.calls.length;
    expect(callback).toBe(1);
  });
});