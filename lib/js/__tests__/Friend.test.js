"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _Friend = _interopRequireDefault(require("../bootstrap-components/Friend"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<Friend />', function () {
  test('Friend component should render', function () {
    var wrapper = (0, _enzyme.shallow)(_react.default.createElement(_Friend.default, null));
    var tree = (0, _enzymeToJson.default)(wrapper);
    expect(tree).toMatchSnapshot();
  });
});