"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _ModalHeader = _interopRequireDefault(require("../bootstrap-components/ModalHeader"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<ModalHeader/>', function () {
  var wrapper = (0, _enzyme.shallow)(_react.default.createElement(_ModalHeader.default, null));
  test('Firstname field should render', function () {
    var tree = (0, _enzymeToJson.default)(wrapper);
    expect(tree).toMatchSnapshot();
  });
});