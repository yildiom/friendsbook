"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _Navigation = _interopRequireDefault(require("../bootstrap-components/Navigation"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<NavigationBar/>', function () {
  test('Navbar should render', function () {
    var navBar = (0, _enzyme.shallow)(_react.default.createElement(_Navigation.default, null));
    var tree = (0, _enzymeToJson.default)(navBar);
    expect(tree).toMatchSnapshot();
  });
});