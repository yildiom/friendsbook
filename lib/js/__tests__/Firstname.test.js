"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _Firstname = _interopRequireDefault(require("../bootstrap-components/Firstname"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<Firstname/>', function () {
  var change = jest.fn();
  var wrapper = (0, _enzyme.shallow)(_react.default.createElement(_Firstname.default, {
    onChange: change
  }));
  test('Firstname field should render', function () {
    var tree = (0, _enzymeToJson.default)(wrapper);
    expect(tree).toMatchSnapshot();
  }); //   test('Firstname field should be changed', () => {
  //     wrapper.find('FormControl').simulate('onChange');
  //     expect(change).toHaveBeenCalled();
  //   });
});