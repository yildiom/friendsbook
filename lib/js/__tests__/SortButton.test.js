"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _SortButton = _interopRequireDefault(require("../bootstrap-components/SortButton"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<SortButton/>', function () {
  var addClick = jest.fn();
  var wrapper = (0, _enzyme.shallow)(_react.default.createElement(_SortButton.default, {
    onClick: addClick
  })); // expect(addClick).NotToHaveBeenCalled();

  test('SortButton should render', function () {
    var tree = (0, _enzymeToJson.default)(wrapper);
    expect(tree).toMatchSnapshot();
  }); //   test('SortButton should be clicked', () => {
  //     wrapper.find('Button').simulate('click');
  //     expect(addClick).toHaveBeenCalled();
  //   });
});