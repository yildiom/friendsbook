"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzyme = require("enzyme");

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _About = _interopRequireDefault(require("../components/About"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<About />', function () {
  test('About component should render', function () {
    var Wrapper = (0, _enzyme.shallow)(_react.default.createElement(_About.default, null));
    var tree = (0, _enzymeToJson.default)(Wrapper);
    expect(tree).toMatchSnapshot();
  });
});