"use strict";

var _react = _interopRequireDefault(require("react"));

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

var _enzyme = require("enzyme");

var _ModalUpdateButton = _interopRequireDefault(require("../bootstrap-components/ModalUpdateButton"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('<ModalUpdateButton />', function () {
  var click = jest.fn();
  var wrapper = (0, _enzyme.shallow)(_react.default.createElement(_ModalUpdateButton.default, {
    onClick: click
  }));
  test('ModalUpdateButton should render', function () {
    var tree = (0, _enzymeToJson.default)(wrapper);
    expect(tree).toMatchSnapshot();
  });
});