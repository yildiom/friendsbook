"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.store = void 0;

var _redux = require("redux");

var _reduxDevtoolsExtension = require("redux-devtools-extension");

var _reduxThunk = _interopRequireDefault(require("redux-thunk"));

var _reduxLogger = require("redux-logger");

var _reduxFirestore = require("redux-firestore");

var _reactReduxFirebase = require("react-redux-firebase");

var _index = _interopRequireDefault(require("../reducers/index"));

var _fbConfig = _interopRequireDefault(require("../config/fbConfig"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Add redux Firebase to compose
// const createStoreWithFirebase = compose(
//   reactReduxFirebase(firebaseConfig),
//   reduxFirestore(firebaseConfig)
// )(createStore);
// export const store = createStoreWithFirebase(
//   combineReducers(reducer),
//   composeWithDevTools(
//     applyMiddleware(
//       thunk.withExtraArgument({ getFirebase, getFirestore }),
//       // reduxFirestore(firebaseConfig),
//       // reactReduxFirebase(firebaseConfig),
//       logger
//     )
//   )
// );
var middlewares = [];
var rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB

};
middlewares.push(_reduxThunk.default.withExtraArgument({
  getFirebase: _reactReduxFirebase.getFirebase,
  getFirestore: _reduxFirestore.getFirestore
}));
middlewares.push((0, _reduxLogger.createLogger)());
var store = (0, _redux.createStore)(_index.default, (0, _redux.compose)(_redux.applyMiddleware.apply(void 0, middlewares), (0, _reduxFirestore.reduxFirestore)(_fbConfig.default), (0, _reactReduxFirebase.reactReduxFirebase)(_fbConfig.default, rrfConfig))); // export const store = createStore(
//   combineReducers(reducer),
//   composeWithDevTools(applyMiddleware(thunk, logger))
// );

exports.store = store;