"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.submitFriendState = void 0;

var _v = _interopRequireDefault(require("uuid/v1"));

var _fbConfig = require("../config/fbConfig");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var submitFriendState = [{
  firstName: 'Martin',
  lastName: 'Scorsese',
  age: 76,
  id: 'firstFriend'
}, {
  firstName: 'Alfred',
  lastName: 'Hitchcock',
  age: 81,
  id: 'secondFriend'
}, {
  firstName: 'Orson',
  lastName: 'Welles',
  age: 70,
  id: 'thirdFriend'
}];
exports.submitFriendState = submitFriendState;
submitFriendState.forEach(function (el) {
  return _fbConfig.db.collection('friends').add(el);
});