"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findByTestAttr = void 0;

var findByTestAttr = function findByTestAttr(component, attr) {
  var wrapper = component.find("[data-test='".concat(attr, "']"));
  return wrapper;
};

exports.findByTestAttr = findByTestAttr;