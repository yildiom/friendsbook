import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Update from '../bootstrap-components/UpdateFriend';

describe('<Update/>', () => {
  // const mockFunc = jest.fn();
  // const wrapper = shallow(<Update modalOpen={mockFunc} />);

  test('UpdateButton should render', () => {
    const wrapper = shallow(<Update />);

    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('UpdateButton should be clicked', () => {
    const modalOpen = jest.fn();

    const wrapper = shallow(<Update modalOpen={modalOpen} />);

    expect(wrapper.state().modalShow).toEqual(false);

    wrapper.find('Button').prop('onClick')();

    expect(wrapper.state().modalShow).toEqual(true);
  });
});
