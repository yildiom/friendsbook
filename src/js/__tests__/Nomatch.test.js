import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Nomatch from '../components/Nomatch';

describe('<Nomatch />', () => {
  test('Nomatch component should render', () => {
    const noMatchShallow = shallow(<Nomatch />);
    const noMatchTree = toJson(noMatchShallow);
    expect(noMatchTree).toMatchSnapshot();
  });
});
