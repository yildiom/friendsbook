import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ModalFirstname from '../bootstrap-components/ModalFirstname';

describe('<ModalFirstname/>', () => {
  const mockFunc = jest.fn();
  const wrapper = shallow(<ModalFirstname updateFirstName={mockFunc} />);
  test('Firstname field should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('Firstname field should be changed', () => {
    wrapper.find('FormControl').simulate('change');
    expect(mockFunc).toHaveBeenCalled();
  });
});
