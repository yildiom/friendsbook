import React from 'react';
// import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';
import Contact from '../components/Contact';
import { findByTestAttr } from '../../Utils/index';

describe('<Contact />', () => {
  test('Contact component should render', () => {
    // const contactShallow = shallow(<Contact />);
    // const contactTree = toJson(contactShallow);
    // expect(contactTree).toMatchSnapshot();
    const wrapper = shallow(<Contact />);
    const contact = findByTestAttr(wrapper, 'contactComponent');
    expect(contact.length).toBe(1);
  });
});
