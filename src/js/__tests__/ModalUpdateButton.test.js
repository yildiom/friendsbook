import React from 'react';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';

import ModalUpdateButton from '../bootstrap-components/ModalUpdateButton';

describe('<ModalUpdateButton />', () => {
  const mockFunc = jest.fn();
  const wrapper = shallow(<ModalUpdateButton onClick={mockFunc} />);

  test('ModalUpdateButton should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  it('Should call the click event', () => {
    wrapper.find('Button').simulate('click');
    expect(mockFunc).toHaveBeenCalled();
  });
});
