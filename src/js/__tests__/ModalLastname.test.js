import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ModalLastname from '../bootstrap-components/ModalLastname';

describe('<ModalLastname/>', () => {
  const mockFunc = jest.fn();
  const wrapper = shallow(<ModalLastname updateLastName={mockFunc} />);
  test('Lastname field should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('Lastname field should be changed', () => {
    wrapper.find('FormControl').simulate('change');
    expect(mockFunc).toHaveBeenCalled();
  });
});
