import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import About from '../components/About';

describe('<About />', () => {
  test('About component should render', () => {
    const Wrapper = shallow(<About />);
    const tree = toJson(Wrapper);
    expect(tree).toMatchSnapshot();
  });
});
