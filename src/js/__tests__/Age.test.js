import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { findByTestAttr } from '../../Utils/index';
import Age from '../bootstrap-components/Age';

describe('<Age/>', () => {
  let wrapper;
  let mockFunc;

  beforeEach(() => {
    mockFunc = jest.fn();
    const props = {
      type: 'age',
      addAge: mockFunc,
    };
    wrapper = shallow(<Age {...props} />);
  });
  test('Age field should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  it('Should have the test attribute', () => {
    const ageComponent = findByTestAttr(wrapper, 'ageComponent');
    expect(ageComponent.length).toBe(1);
  });

  it('Should call the callback change event', () => {
    const ageComponent = findByTestAttr(wrapper, 'ageComponent');
    // const event = { target: { value: 'sometext' } };
    ageComponent.simulate('change');
    const callback = mockFunc.mock.calls.length;
    expect(callback).toEqual(1);
    // expect(mockFunc).toBeCalledWith('sometext');
  });
});
