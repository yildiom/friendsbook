import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { findByTestAttr } from '../../Utils';
import DeleteButton from '../bootstrap-components/DeleteButton';

describe('<DeleteButton/>', () => {
  let wrapper;
  let mockFunc;

  beforeEach(() => {
    mockFunc = jest.fn();
    const props = {
      id: 'id',
      deleteFriend: mockFunc,
    };
    wrapper = shallow(<DeleteButton {...props} />);
  });

  test('AddButton should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });

  test('Check if it has attribute', () => {
    const deleteButton = findByTestAttr(wrapper, 'deleteButton');
    expect(deleteButton.length).toBe(1);
  });

  it('DeleteButton should be clicked', () => {
    const button = findByTestAttr(wrapper, 'deleteButton');
    button.simulate('click');
    // const callback = mockFunc.mock.calls.length;
    // expect(callback).toEqual(1); ===> THIS EXPECT STATEMENT ALSO PASSES
    expect(mockFunc).toHaveBeenCalled();
  });
});
