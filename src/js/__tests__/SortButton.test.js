import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import SortButton from '../bootstrap-components/SortButton';

describe('<SortButton/>', () => {
  const mockFunc = jest.fn();
  const wrapper = shallow(<SortButton sortFriends={mockFunc} />);
  test('SortButton should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('SortButton should be clicked', () => {
    wrapper.find('Button').simulate('click');
    expect(mockFunc).toHaveBeenCalled();
  });
});
