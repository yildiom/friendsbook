import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ModalAge from '../bootstrap-components/ModalAge';

describe('<ModalAge/>', () => {
  const mockFunc = jest.fn();
  const wrapper = shallow(<ModalAge updateAge={mockFunc} />);
  test('Age field should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('Age field should be changed', () => {
    wrapper.find('FormControl').simulate('change');
    expect(mockFunc).toHaveBeenCalled();
  });
});
