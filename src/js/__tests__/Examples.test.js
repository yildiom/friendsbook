// import { subtract, total } from '../Examples';

// Mock function: Redefining a function specifically for a test to generate a result.
// E.g. returning hard-coded data instead of waiting for fetch requests or database calls.
const add = jest.fn((x, y) => x + y);
const subtract = jest.fn((x, y) => x - y);

// UNIT TEST ==> it tests one thing, one component// we use enzymes shallow to do unit testing
// it and test are doing the same thing
// Describe is something optional to wrap tests

function Person(name, foods) {
  this.name = name;
  this.foods = foods;
}

Person.prototype.fetchFavFoods = function() {
  return new Promise((resolve, reject) => {
    // Simulate an API
    setTimeout(() => resolve(this.foods), 2000);
  });
};

describe('Doing basic Math to learn Jest', () => {
  it('add', () => {
    // expect is an assertion. It always checks if the return of the entered function is true
    // toEqual or toBe is a matcher. Depending on the assertion resulting value of a matcher needs to be true.
    expect(add(1, 2)).toEqual(3);
    expect(add).toHaveBeenCalledTimes(1);
    expect(add).toHaveBeenCalledWith(1, 2);
    // toBe means exact equality
    expect(3 + 5).toBe(8);
  });

  test('subtract', () => {
    const value = subtract(4, 2);
    expect(value).toBe(2);
  });

  // Integration tests
  // Tests things working together
  // We use enzymes mount for integration test so that child components are also tested
  // test('total', () => {
  //   expect(total(5, 20)).toBe('$25');
  // });
});

describe('mocking learning', () => {
  it('can create a person', () => {
    const me = new Person('Omer', ['kebap', 'mangal']);
    expect(me.name).toBe('Omer');
  });

  // it('can fetch foods', async () => {
  //   const me = new Person('Omer', ['kebap', 'mangal']);
  //   //     // mock the favFoods function
  //   me.fetchFavFoods = jest.fn().mockResolvedValue(['sushi', 'ramen']);
  //   const favFoods = await me.fetchFavFoods();
  //   expect(favFoods).toContain('kebap');
  // });
});
