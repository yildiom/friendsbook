import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import NavigationBar from '../bootstrap-components/Navigation';

describe('<NavigationBar/>', () => {
  test('Navbar should render', () => {
    const navBar = shallow(<NavigationBar />);
    const tree = toJson(navBar);
    expect(tree).toMatchSnapshot();
  });
});
