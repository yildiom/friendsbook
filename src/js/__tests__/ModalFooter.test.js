import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ModalFooter from '../bootstrap-components/ModalFooter';

describe('<ModalFooter/>', () => {
  const mockFunc = jest.fn();
  const wrapper = shallow(<ModalFooter hide={mockFunc} />);
  test('CloseButton should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('CloseButton should be clicked', () => {
    wrapper.find('Button').simulate('click');
    expect(mockFunc).toHaveBeenCalled();
  });
});
