import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Firstname from '../bootstrap-components/Firstname';

describe('<Firstname/>', () => {
  const mockFunc = jest.fn();
  const props = {
    type: 'firstName',
    addFirstName: mockFunc,
  };

  const wrapper = shallow(<Firstname {...props} />);
  test('Firstname field should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('Firstname field should be changed', () => {
    const firstNameComp = wrapper.find('FormControl');
    firstNameComp.simulate('change');
    // const callback = mockFunc.mock.calls.length;
    // expect(callback).toEqual(1);
    expect(mockFunc).toHaveBeenCalled();
  });
});
