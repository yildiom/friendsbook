import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ModalHeader from '../bootstrap-components/ModalHeader';

describe('<ModalHeader/>', () => {
  const wrapper = shallow(<ModalHeader />);
  test('ModalHeader field should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
});
