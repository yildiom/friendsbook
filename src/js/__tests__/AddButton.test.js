import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { findByTestAttr } from '../../Utils/index';

import AddButton from '../bootstrap-components/AddButton';

describe('<AddButton/>', () => {
  let wrapper;
  let addClick;

  beforeEach(() => {
    addClick = jest.fn();
    wrapper = shallow(<AddButton onClick={addClick} />);
  });

  test('AddButton should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('Check if it has attribute', () => {
    const button = findByTestAttr(wrapper, 'AddButton');
    expect(button.length).toBe(1);
  });

  it('Should call the click event', () => {
    const button = findByTestAttr(wrapper, 'AddButton');
    button.simulate('click');
    const callback = addClick.mock.calls.length;
    expect(callback).toBe(1);
  });
});
