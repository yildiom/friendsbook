import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import Lastname from '../bootstrap-components/Lastname';

describe('<Lastname/>', () => {
  const mockFunc = jest.fn();
  const props = {
    type: 'lastName',
    addLastName: mockFunc,
  };
  const wrapper = shallow(<Lastname {...props} />);
  test('Lastname field should render', () => {
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
  test('Lastname field should be changed', () => {
    const lastNameComp = wrapper.find('FormControl');
    lastNameComp.simulate('change');
    expect(mockFunc).toHaveBeenCalled();
  });
});
