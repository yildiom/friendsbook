import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Friend from '../bootstrap-components/Friend';

describe('<Friend />', () => {
  test('Friend component should render', () => {
    const wrapper = shallow(<Friend />);
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
});
