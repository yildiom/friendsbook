// @flow

import React from 'react';
import { Form, Col, Row } from 'react-bootstrap';

type Props = {
  firstName: string,
  updateFirstName: (e: *) => void,
};

const ModalFirstname = ({ firstName, updateFirstName }: Props) => (
  <Form.Group as={Row} controlId="formPlaintextPassword">
    <Form.Label column sm="2">
      Firstname
    </Form.Label>
    <Col sm="10">
      <Form.Control
        type="firstname"
        placeholder={firstName}
        onChange={updateFirstName}
      />
    </Col>
  </Form.Group>
);

export default ModalFirstname;
