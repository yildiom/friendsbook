/* eslint-disable react/destructuring-assignment */

import React from 'react';
import { Button, ButtonToolbar } from 'react-bootstrap';
import PropTypes from 'prop-types';
import MyVerticallyCenteredModal from '../containers/MyVerticallyCenteredModal';

class Update extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = { modalShow: false };
  }

  render() {
    const modalClose = () => this.setState({ modalShow: false });
    const modalOpen = () => this.setState({ modalShow: true });

    return (
      <ButtonToolbar>
        <Button
          variant="primary"
          style={{ marginLeft: '10px' }}
          // onClick={() => this.setState({ modalShow: true })}
          onClick={modalOpen}
        >
          Update
        </Button>

        <MyVerticallyCenteredModal
          show={this.state.modalShow}
          onHide={modalClose}
          id={this.props.id}
        />
      </ButtonToolbar>
    );
  }
}

Update.propTypes = {
  id: PropTypes.string,
};

export default Update;

// <UpdateButton modalOpen={modalOpen}/>
