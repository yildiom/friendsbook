// @flow

import React from 'react';
import { Button } from 'react-bootstrap';

type Props = {
  onClick: Function,
};

const AddButton = ({ onClick }: Props) => (
  <Button
    variant="primary"
    type="submit"
    onClick={onClick}
    data-test="AddButton"
  >
    Add
  </Button>
);

export default AddButton;
