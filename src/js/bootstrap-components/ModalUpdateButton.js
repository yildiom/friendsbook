// @flow

import React from 'react';
import { Button } from 'react-bootstrap';

type Props = {
  onClick: Function,
};

const ModalUpdateButton = ({ onClick }: Props) => (
  <Button variant="primary" type="submit" onClick={onClick}>
    Update
  </Button>
);

export default ModalUpdateButton;
