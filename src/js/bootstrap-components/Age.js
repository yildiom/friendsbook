// @flow

import React from 'react';
import { Form } from 'react-bootstrap';

type Props = {
  addAge: (e: *) => void,
};
const Age = ({ addAge }: Props) => (
  <Form.Group controlId="formBasicPassword">
    <Form.Label>Age</Form.Label>
    <Form.Control
      type="age"
      placeholder="Enter Age"
      onChange={addAge}
      data-test="ageComponent"
    />
  </Form.Group>
);

export default Age;
