// @flow

import React from 'react';
import { Form } from 'react-bootstrap';

type Props = {
  addFirstName: (e: *) => void,
};

const Firstname = ({ addFirstName }: Props) => (
  <Form.Group controlId="formBasicEmail">
    <Form.Label>Firstname</Form.Label>
    <Form.Control
      type="firstname"
      placeholder="Enter Firstname"
      onChange={addFirstName}
    />
  </Form.Group>
);

export default Firstname;
