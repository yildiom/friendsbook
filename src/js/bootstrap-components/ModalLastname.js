// @flow

import React from 'react';
import { Form, Col, Row } from 'react-bootstrap';

type Props = {
  lastName: string,
  updateLastName: (e: *) => void,
};

const ModalLastname = ({ lastName, updateLastName }: Props) => (
  <Form.Group as={Row} controlId="formPlaintextPassword">
    <Form.Label column sm="2">
      Lastname
    </Form.Label>
    <Col sm="10">
      <Form.Control
        type="lastname"
        placeholder={lastName}
        onChange={updateLastName}
      />
    </Col>
  </Form.Group>
);

export default ModalLastname;
