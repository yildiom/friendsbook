// @flow

import React from 'react';
import { Link } from 'react-router-dom';
import { Nav, Navbar, Container } from 'react-bootstrap';
import { connect } from 'react-redux';

const NavigationBar = () => (
  <Container>
    <Navbar expand="lg" bg="primary" variant="dark">
      <Navbar.Brand>
        <Link to="/">Friendsbook</Link>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Item>
            <Link to="/">Home</Link>
          </Nav.Item>
          <Nav.Item>
            <Link to="/about">About</Link>
          </Nav.Item>
          <Nav.Item>
            <Link to="/contact">Contact</Link>
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </Container>
);

// const mapStateToProps = state => {
//   console.log(state);
// };

// export default connect(mapStateToProps)(NavigationBar);

export default NavigationBar;
