// @flow

import React from 'react';
import { Modal } from 'react-bootstrap';

const ModalHeader = () => (
  <Modal.Header closeButton>
    <Modal.Title id="contained-modal-title-vcenter">
      Update Friend Info
    </Modal.Title>
  </Modal.Header>
);

export default ModalHeader;
