// @flow

import React from 'react';
import { Button, Modal } from 'react-bootstrap';

type Props = {
  hide: (e: *) => void,
};

const ModalFooter = ({ hide }: Props) => (
  <Modal.Footer>
    <Button
      variant="danger"
      onClick={hide}
      style={{ fontFamily: 'Montserrat' }}
    >
      Close
    </Button>
  </Modal.Footer>
);

export default ModalFooter;
