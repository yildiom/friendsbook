// @flow

import React from 'react';
import { Form } from 'react-bootstrap';

type Props = {
  addLastName: (e: *) => void,
};

const Lastname = ({ addLastName }: Props) => (
  <Form.Group controlId="formBasicPassword">
    <Form.Label>Lastname</Form.Label>
    <Form.Control
      type="lastname"
      placeholder="Enter Lastname"
      onChange={addLastName}
    />
  </Form.Group>
);

export default Lastname;
