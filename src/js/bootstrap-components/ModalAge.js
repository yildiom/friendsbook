// @flow

import React from 'react';
import { Form, Col, Row } from 'react-bootstrap';

type Props = {
  age: number,
  updateAge: (e: *) => void,
};

const ModalAge = ({ age, updateAge }: Props) => (
  <Form.Group as={Row} controlId="formPlaintextPassword">
    <Form.Label column sm="2">
      Age
    </Form.Label>
    <Col sm="10">
      <Form.Control type="lastname" placeholder={age} onChange={updateAge} />
    </Col>
  </Form.Group>
);

export default ModalAge;
