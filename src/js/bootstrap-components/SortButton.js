// @flow

import React from 'react';
import { Button } from 'react-bootstrap';

type Props = {
  sortFriends: (e: *) => void,
};

const SortButton = ({ sortFriends }: Props) => (
  <div className="sort">
    <Button variant="secondary" onClick={sortFriends}>
      Sort
    </Button>
  </div>
);

export default SortButton;
