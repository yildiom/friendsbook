// @flow

import React from 'react';
import { Button } from 'react-bootstrap';

type Props = {
  deleteFriend: Function,
  id: string,
};

const DeleteButton = ({ deleteFriend, id }: Props) => (
  <Button
    variant="danger"
    onClick={deleteFriend}
    id={id}
    data-test="deleteButton"
  >
    Delete
  </Button>
);

export default DeleteButton;
