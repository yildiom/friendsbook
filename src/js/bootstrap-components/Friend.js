// @flow

import React from 'react';
import { Card } from 'react-bootstrap';
import Update from './UpdateFriend';
import DeleteButton from './DeleteButton';

type Props = {
  deleteFriend: (e: *) => void,
  id: string,
  name: string,
  lastName: string,
  age: number,
};

const Friend = ({ id, name, lastName, age, deleteFriend }: Props) => (
  <li key={id}>
    <Card>
      <Card.Body>
        <Card.Title>
          <span className="name">
            Name: {name} {lastName}
          </span>
          <span className="age"> Age: {age}</span>
        </Card.Title>
        <div className="buttonHolder">
          <DeleteButton deleteFriend={deleteFriend} id={id} />
          <Update id={id} />
        </div>
      </Card.Body>
    </Card>
  </li>
);

export default Friend;
