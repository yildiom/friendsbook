import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { reduxFirestore, getFirestore } from 'redux-firestore';
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase';
import reducer from '../reducers/index';
import firebase from '../config/fbConfig';

// Add redux Firebase to compose
// const createStoreWithFirebase = compose(
//   reactReduxFirebase(firebaseConfig),
//   reduxFirestore(firebaseConfig)
// )(createStore);

// export const store = createStoreWithFirebase(
//   combineReducers(reducer),
//   composeWithDevTools(
//     applyMiddleware(
//       thunk.withExtraArgument({ getFirebase, getFirestore }),
//       // reduxFirestore(firebaseConfig),
//       // reactReduxFirebase(firebaseConfig),
//       logger
//     )
//   )
// );

const middlewares = [];

const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true, // Firestore for Profile instead of Realtime DB
};

middlewares.push(thunk.withExtraArgument({ getFirebase, getFirestore }));
middlewares.push(createLogger());

export const store = createStore(
  reducer,
  compose(
    applyMiddleware(...middlewares),
    reduxFirestore(firebase),
    reactReduxFirebase(firebase, rrfConfig)
  )
);

// export const store = createStore(
//   combineReducers(reducer),
//   composeWithDevTools(applyMiddleware(thunk, logger))
// );
