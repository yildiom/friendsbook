// @flow
import uuidv1 from 'uuid/v1';
import { type Friend } from '../reducers/Friend.d';
import { db } from '../config/fbConfig';

type InitialState = Friend[];
export const submitFriendState: InitialState = [
  {
    firstName: 'Martin',
    lastName: 'Scorsese',
    age: 76,
    id: 'firstFriend',
  },

  {
    firstName: 'Alfred',
    lastName: 'Hitchcock',
    age: 81,
    id: 'secondFriend',
  },
  {
    firstName: 'Orson',
    lastName: 'Welles',
    age: 70,
    id: 'thirdFriend',
  },
];

submitFriendState.forEach(el => db.collection('friends').add(el));
