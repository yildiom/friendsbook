// @flow

import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Layout from '../bootstrap-components/Layout';
import Routes from '../components/Routes';

import Navigationbar from '../bootstrap-components/Navigation';

const App = () => (
  <>
    <Router>
      <Navigationbar />
      <Layout>
        <Routes />
      </Layout>
    </Router>
  </>
);

export default App;
