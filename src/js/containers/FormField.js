// @flow
/* eslint-disable no-shadow */

import React from 'react';
import { connect } from 'react-redux';
import { State, Dispatch, compose } from 'redux';
import uuidv1 from 'uuid/v1';
import { firestoreConnect, firebaseConnect } from 'react-redux-firebase';
import {
  addFirstName,
  addLastName,
  addAge,
  // emptyForm,
} from '../actions/AddFriendAction';
import { submitFriend } from '../actions/submitFriendAction';
import AddButton from '../bootstrap-components/AddButton';
import Firstname from '../bootstrap-components/Firstname';
import Lastname from '../bootstrap-components/Lastname';
import Age from '../bootstrap-components/Age';

type Actions = {
  addFirstName: (e: *) => void,
  addLastName: (e: *) => void,
  addAge: (e: *) => void,
  submitFriend: (e: *) => void,
};

const FormField = ({
  addFirstName,
  addLastName,
  addAge,
  submitFriend,
}: // emptyForm,
Actions) => (
  <>
    <fieldset>
      <legend>Add a friend</legend>
      <form action="" onSubmit={submitFriend}>
        <Firstname addFirstName={addFirstName} />
        <Lastname addLastName={addLastName} />
        <Age addAge={addAge} />
        <AddButton />
      </form>
    </fieldset>
  </>
);

const mapDispatchToProps = dispatch => ({
  addFirstName: (e: *) => {
    dispatch(addFirstName(e.target.value));
  },
  addLastName: (e: *) => {
    dispatch(addLastName(e.target.value));
  },
  addAge: (e: *) => {
    dispatch(addAge(parseInt(e.target.value, 10)));
  },
  submitFriend: (e: *) => {
    e.preventDefault();
    const id = uuidv1();
    dispatch(submitFriend(id));
    // dispatch(emptyForm());
  },
  // emptyForm: (e: *) => {
  //   e.preventDefault();
  //   dispatch(emptyForm());
  // },
});

export default compose(
  firestoreConnect([{ collection: 'friends' }]),
  connect(
    null,
    mapDispatchToProps
  )
)(FormField);

// export default connect<State, Dispatch>(
//   null,
//   mapDispatchToProps
// )(FormField);
