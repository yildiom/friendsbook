import React from 'react';
import FormField from './FormField';
import FriendsGrid from './FriendsGrid';

const Home = () => (
  <div className="main">
    <FormField />
    <FriendsGrid />
  </div>
);

export default Home;
