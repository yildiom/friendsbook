/* eslint-disable no-shadow */
// @flow

import React from 'react';
import { connect } from 'react-redux';
import { State, Dispatch, compose } from 'redux';
import { firestoreConnect, firebaseConnect } from 'react-redux-firebase';
import { deleteFriend } from '../actions/DeleteFriendAction';
import { sortFriends } from '../actions/SortFriendsAction';
import Friend from '../bootstrap-components/Friend';
import SortButton from '../bootstrap-components/SortButton';
import { db } from '../config/fbConfig';

type Props = {
  friendList: Array<Object>,
  deleteFriend: (e: *) => void,
  sortFriends: (e: *) => void,
  // friends: Array<Object>,
};

const FriendsGrid = ({
  // friends,
  friendList,
  deleteFriend,
  sortFriends,
}: Props) => (
  <ul>
    {friendList.map(el => (
      <Friend
        id={el.id}
        key={el.id}
        name={el.firstName}
        lastName={el.lastName}
        age={el.age}
        deleteFriend={deleteFriend}
      />
    ))}
    <SortButton className="sort" sortFriends={sortFriends} />
  </ul>
);

// const mapStateToProps = state =>
//   // console.log(state);
//   console.log(state.firestore.ordered.friends);
// ({
//   friendList: state.friendList,
//   friends: state.firestore.ordered.friends,
//   isLoaded: state.firebase.profile.isLoaded,
// });

const mapStateToProps = ({ friendList, firestore }) => ({
  friendList,
  friends: firestore.ordered.friends,
});

const mapDispatchToProps = (dispatch: Function) => ({
  deleteFriend: e => {
    e.persist();
    e.preventDefault();
    dispatch(deleteFriend(e.target.id));
  },
  sortFriends: e => {
    e.preventDefault();
    dispatch(sortFriends());
  },
});

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(FriendsGrid);

export default compose(
  firestoreConnect([{ collection: 'friends' }]),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(FriendsGrid);

// db
//   .collection('friends')
//   .get()
//   .then(function (result) {
//     const emptyArr = [];
//     result.forEach(function (doc) {
//       emptyArr.push(doc.data());
//     });
//     emptyArr.map(el => (
//       <Friend
//         id={el.id}
//         key={el.id}
//         name={el.firstName}
//         lastName={el.lastName}
//         age={el.age}
//         deleteFriend={deleteFriend}
//       />
//     ));
//   })
//     }
//     <SortButton className="sort" sortFriends={sortFriends} />
//   </ul>
// );
