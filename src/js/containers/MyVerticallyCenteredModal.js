/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  updateFirstName,
  updateLastName,
  updateAge,
} from '../actions/UpdateFriendAction';
import { submitUpdate } from '../actions/SubmitUpdateAction';
import ModalFirstname from '../bootstrap-components/ModalFirstname';
import ModalLastname from '../bootstrap-components/ModalLastname';
import ModalAge from '../bootstrap-components/ModalAge';
import ModalUpdateButton from '../bootstrap-components/ModalUpdateButton';
import ModalHeader from '../bootstrap-components/ModalHeader';
import ModalFooter from '../bootstrap-components/ModalFooter';

class MyVerticallyCenteredModal extends React.Component {
  constructor(props) {
    super(props);
    this.id = props.id;
    this.friendList = props.friendList;
    this.submitUpdate = props.submitUpdate;
    this.user = this.friendList.filter(usr => usr.id === this.id);
    this.updateAge = props.updateAge;
    this.updateFirstName = props.updateFirstName;
    this.updateLastName = props.updateLastName;
  }

  static get propTypes() {
    return {
      onHide: PropTypes.func,
      id: PropTypes.string,
      friendList: PropTypes.array,
      updateFirstName: PropTypes.func,
      updateLastName: PropTypes.func,
      updateAge: PropTypes.func,
      submitUpdate: PropTypes.func,
    };
  }

  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <ModalHeader />

        <Modal.Body>
          <form action="" onSubmit={this.submitUpdate} id={this.id}>
            <ModalFirstname
              firstName={this.user[0].firstName}
              updateFirstName={this.updateFirstName}
            />
            <ModalLastname
              lastName={this.user[0].lastName}
              updateLastName={this.updateLastName}
            />
            <ModalAge age={this.user[0].age} updateAge={this.updateAge} />
            <ModalUpdateButton id={this.id} />
          </form>
        </Modal.Body>

        <ModalFooter hide={this.props.onHide} />
      </Modal>
    );
  }
}

const mapStateToProps = ({ friendList }) => ({
  friendList,
});

const mapDispatchToProps = dispatch => ({
  updateFirstName: e => {
    dispatch(updateFirstName(e.target.value));
  },
  updateLastName: e => {
    dispatch(updateLastName(e.target.value));
  },
  updateAge: e => {
    dispatch(updateAge(parseInt(e.target.value, 10)));
  },
  submitUpdate: e => {
    e.preventDefault();
    e.persist();
    dispatch(submitUpdate(e.target.id));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyVerticallyCenteredModal);
