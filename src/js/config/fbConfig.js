import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyARg5rVBYe0Fo8Wyj2KizFke_dummX_DZk',
  authDomain: 'friendsbook-8ec44.firebaseapp.com',
  databaseURL: 'https://friendsbook-8ec44.firebaseio.com',
  projectId: 'friendsbook-8ec44',
  storageBucket: '',
  messagingSenderId: '454979822945',
  appId: '1:454979822945:web:a0e7f1637a1476af',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export const db = firebase.firestore();

export default firebase;
