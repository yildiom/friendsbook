// @flow
export type Friend = {
  firstName: string,
  lastName: string,
  age: number,
  id: string | number,
};
