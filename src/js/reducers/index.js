// @flow

import { firestoreReducer } from 'redux-firestore';
import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase';
import { friendInfoReducer } from './AddFriendReducer';
import { friendReducer } from './FriendReducer';

const reducer = combineReducers({
  firestore: firestoreReducer,
  firebase: firebaseReducer,
  friend: friendInfoReducer,
  friendList: friendReducer,
});
export default reducer;
