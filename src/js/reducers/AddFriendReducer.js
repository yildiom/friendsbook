// @flow

import { friendState } from '../store/friendState';

export const friendInfoReducer = (
  state: Object = friendState,
  action: Object
) => {
  switch (action.type) {
    case 'ADD_FIRSTNAME':
      return {
        ...state,
        firstName: action.payload,
      };
    case 'ADD_LASTNAME':
      return {
        ...state,
        lastName: action.payload,
      };
    case 'ADD_AGE':
      return {
        ...state,
        age: action.payload,
      };
    case 'EMPTY_FORM':
      return {
        // ...state,
        // firstName: '',
        // lastName: '',
        // age: '',
        state,
      };

    default:
      return state;
  }
};
