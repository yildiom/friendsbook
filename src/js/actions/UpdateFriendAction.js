// @flow

import { UPDATE_FIRSTNAME, UPDATE_LASTNAME, UPDATE_AGE } from './actionsTypes';

export const updateFirstName = (updatedFirstname: string) => ({
  type: UPDATE_FIRSTNAME,
  payload: updatedFirstname,
});

export const updateLastName = (updatedLastname: string) => ({
  type: UPDATE_LASTNAME,
  payload: updatedLastname,
});

export const updateAge = (updatedAge: number) => ({
  type: UPDATE_AGE,
  payload: updatedAge,
});
