// @flow

import { SUBMIT_UPDATE } from './actionsTypes';

export const submitUpdate = (id: string) => (dispatch: any, getState: any) =>
  dispatch({
    type: SUBMIT_UPDATE,
    payload: { ...getState().friend, id },
  });
