// @flow

import {
  ADD_FIRSTNAME,
  ADD_LASTNAME,
  ADD_AGE,
  EMPTY_FORM,
} from './actionsTypes';

export const addFirstName = (firstname: string) => ({
  type: ADD_FIRSTNAME,
  payload: firstname,
});

export const addLastName = (lastname: string) => ({
  type: ADD_LASTNAME,
  payload: lastname,
});

export const addAge = (age: number) => ({
  type: ADD_AGE,
  payload: age,
});

export const emptyForm = () => ({
  type: EMPTY_FORM,
});
