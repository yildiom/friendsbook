// @flow

import { SUBMIT_FRIEND } from './actionsTypes';
import { db } from '../config/fbConfig';

// export const submitFriend = (id: string) => (dispatch: any, getState: any) =>
//   dispatch({
//     type: SUBMIT_FRIEND,
//     payload: { ...getState().friend, id },
//   });

export const submitFriend = (id: string) => (dispatch: any, getState: any) => {
  db.collection('friends')
    .add({
      ...getState().friend,
      id,
    })
    .then(() => {
      dispatch({
        type: SUBMIT_FRIEND,
        payload: { ...getState().friend, id },
      });
    });
};
