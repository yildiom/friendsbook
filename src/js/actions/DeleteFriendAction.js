// @flow

import { DELETE_FRIEND } from './actionsTypes';
import { db } from '../config/fbConfig';

export const deleteFriend = (id: string) => (dispatch: any) => {
  const query = db.collection('friends').where('id', '==', id);
  query
    .get()
    .then(function(snapShot) {
      snapShot.forEach(function(doc) {
        doc.ref.delete();
      });
    })
    .then(() => {
      dispatch({
        type: DELETE_FRIEND,
        payload: id,
      });
    });
};
