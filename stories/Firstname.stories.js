import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Firstname from '../src/js/bootstrap-components/Firstname';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('Firstname', () => (
  <Firstname onChange={action('Firstname is entered')} />
));
