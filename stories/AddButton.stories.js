import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import AddButton from '../src/js/bootstrap-components/AddButton';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('AddButton', () => (
  <AddButton onClick={action('Add Button clicked')} />
));
