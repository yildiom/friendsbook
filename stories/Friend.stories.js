import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Card, Button, Container } from 'react-bootstrap';

import Friend from '../src/js/bootstrap-components/Friend';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('Friend', () => (
  <li>
    <Card>
      <Card.Body>
        <Card.Title>
          <span className="name">Name: Martin Scorsese</span>
          <span className="age"> Age: 76 </span>
        </Card.Title>
      </Card.Body>
    </Card>
  </li>
));
