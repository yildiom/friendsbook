import React from 'react';
import { storiesOf } from '@storybook/react';
import About from '../src/js/components/About';
import Contact from '../src/js/components/Contact';

storiesOf('/Components', module)
  .add('about', () => <About />)
  .add('contact', () => <Contact />);
