import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DeleteButton from '../src/js/bootstrap-components/DeleteButton';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('DeleteButton', () => (
  <DeleteButton onClick={action('Delete Button clicked')} />
));
