import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import ModalFirstname from '../src/js/bootstrap-components/ModalFirstname';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('ModalFirstname', () => (
  <ModalFirstname onChange={action('Firstname is updated')} />
));
