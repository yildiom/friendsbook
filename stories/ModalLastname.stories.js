import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import ModalLastname from '../src/js/bootstrap-components/ModalLastname';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('ModalLastname', () => (
  <ModalLastname onChange={action('Lastname is updated')} />
));
