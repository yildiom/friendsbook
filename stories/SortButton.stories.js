import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import SortButton from '../src/js/bootstrap-components/SortButton';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('SortButton', () => (
  <SortButton onClick={action('Friends are sorted')} />
));
