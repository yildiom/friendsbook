import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Form, Button, Container } from 'react-bootstrap';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('FormField', () => (
  <Container>
    <fieldset>
      <legend>Add a friend</legend>
      <form
        action=""
        onSubmit={function(e) {
          action('submitted');
          e.preventDefault();
        }}
      >
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Firstname</Form.Label>
          <Form.Control
            type="firstname"
            placeholder="Enter Firstname"
            onChange={action('Firstname entered')}
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Lastname</Form.Label>
          <Form.Control
            type="lastname"
            placeholder="Enter Lastname"
            onChange={action('Lastname entered')}
          />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Age</Form.Label>
          <Form.Control
            type="age"
            placeholder="Enter Age"
            onChange={action('Age entered')}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Add
        </Button>
      </form>
    </fieldset>
  </Container>
));
