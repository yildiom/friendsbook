import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Age from '../src/js/bootstrap-components/Age';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('Age', () => (
  <Age onChange={action('Age is entered')} />
));
