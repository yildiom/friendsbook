import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import ModalAge from '../src/js/bootstrap-components/ModalAge';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('ModalAge', () => (
  <ModalAge onChange={action('Age is updated')} />
));
