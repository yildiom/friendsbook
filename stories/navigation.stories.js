import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Link, BrowserRouter as Router } from 'react-router-dom';
import { Nav, Navbar, Container } from 'react-bootstrap';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('NavigationBar', () => (
  <Container>
    <Router>
      <Navbar expand="lg" bg="primary" variant="dark">
        <Navbar.Brand onClick={action('Home clicked')}>
          <Link to="/">Friendsbook</Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Item onClick={action('Home clicked')}>
              <Link to="/">Home</Link>
            </Nav.Item>
            <Nav.Item onClick={action('About clicked')}>
              <Link to="/about">About</Link>
            </Nav.Item>
            <Nav.Item onClick={action('Contact clicked')}>
              <Link to="/contact">Contact</Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </Router>
  </Container>
));
