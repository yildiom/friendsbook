import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ModalUpdateButton from '../src/js/bootstrap-components/ModalUpdateButton';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('ModalUpdateButton', () => (
  <ModalUpdateButton onClick={action('Friend is updated')} />
));
