import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Lastname from '../src/js/bootstrap-components/Lastname';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('Lastname', () => (
  <Lastname onChange={action('Lastname is entered')} />
));
