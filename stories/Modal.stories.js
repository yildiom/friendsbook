import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Button, Modal, Form, Col, Row } from 'react-bootstrap';

import '../dist/js.00a46daa.css';

const submitAction = action('Update submitted');

storiesOf('Bootstrap-Components', module).add('Modal', () => (
  <Modal.Dialog>
    <Modal.Header closeButton>
      <Modal.Title id="contained-modal-title-vcenter">
        Update Friend Info
      </Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <form
        onSubmit={e => {
          e.preventDefault();
          submitAction(e);
        }}
      >
        <Form.Group as={Row} controlId="formPlaintextPassword">
          <Form.Label column sm="2">
            Firstname
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="firstname"
              onChange={action('Firstname updated')}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formPlaintextPassword">
          <Form.Label column sm="2">
            Lastname
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="lastname"
              onChange={action('Lastname updated')}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formPlaintextPassword">
          <Form.Label column sm="2">
            Age
          </Form.Label>
          <Col sm="10">
            <Form.Control type="age" onChange={action('Age updated')} />
          </Col>
        </Form.Group>
        <Button variant="primary" type="submit">
          Update
        </Button>
      </form>
    </Modal.Body>
    <Modal.Footer>
      <Button onClick={action('Close clicked')}>Close</Button>
    </Modal.Footer>
  </Modal.Dialog>
));
