import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Card, Button, Container } from 'react-bootstrap';
import FriendsGrid from '../src/js/containers/FriendsGrid';
import { submitFriendState } from '../src/js/store/submitFriendState';

import '../dist/js.00a46daa.css';

storiesOf('/Bootstrap-Components', module).add('FriendsGrid', () => (
  <Container>
    <ul>
      {submitFriendState.map(el => (
        <li key={el.id}>
          <Card>
            <Card.Body>
              <Card.Title>
                <span className="name">
                  Name: {el.firstName} {el.lastName}
                </span>
                <span className="age"> Age: {el.age}</span>
              </Card.Title>
              <div className="buttonHolder">
                <Button
                  variant="primary"
                  onClick={action('Delete clicked')}
                  id={el.id}
                >
                  Delete
                </Button>

                <Button
                  variant="primary"
                  className="update"
                  onClick={action('Update clicked')}
                >
                  Update
                </Button>
              </div>
            </Card.Body>
          </Card>
        </li>
      ))}
      <div className="sort">
        <Button variant="primary" onClick={action('Sort clicked')}>
          Sort
        </Button>
      </div>
    </ul>
  </Container>
));
